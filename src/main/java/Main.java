import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) throws IOException {
        String PATH_SEP = File.separator;
        String pathFile = "file.txt";
        Path pathDir = Paths.get("backup");
        Files.createDirectories(pathDir);

        Path filePath = Paths.get(pathDir + PATH_SEP + pathFile);

        if (Files.exists(Paths.get(String.valueOf(filePath)))) {
            Files.delete(Paths.get(String.valueOf(filePath)));
        }

        String data = "hello " + LocalDateTime.now();
        //String data = "hello";

        Files.createFile(filePath);
        Files.write(filePath, data.getBytes());
    }
}
